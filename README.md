Firstly make sure that nvm is installed (https://github.com/nvm-sh/nvm) then run `nvm install` to swap to the correct node version.
Then run

```shell
npm install
npm run build
npm run link-cli
```

to install the dependencies and link the package to the cli command.
Finally run

```shell
marsRobot start
```

You will be prompted to enter the size of the grid, starting coordinates and instructions for each robot.
You will then be prompted to add another robot or end the program.
Ending the program will display the final positions of each robot.

To run the tests, run

```shell
npm run test
```
