import { Robot } from './Robot.js';
import { Instructions, Orientation, Position } from './validators.js';

export class Mars {
  upperX: number;
  upperY: number;
  robots: Robot[] = [];
  scentLocations: Position[] = [];

  constructor(upperX: number, upperY: number) {
    this.upperX = upperX;
    this.upperY = upperY;
  }

  addRobot(startPosition: { x: number; y: number; orientation: Orientation }, instructions: Instructions) {
    const robot = new Robot(startPosition, instructions);
    robot.executeInstructions(this.scentLocations, this.upperX, this.upperY);
    this.robots.push(robot);
  }

  displayResults() {
    this.robots.forEach((robot, index) => {
      console.log(
        `Robot ${index + 1}: ${robot.position.x} ${robot.position.y} ${robot.position.orientation}${robot.lost ? ' LOST' : ''}`
      );
    });
  }
}
