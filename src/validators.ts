import { z } from 'zod';

export const MarsCoordinatesSchema = z
  .object({
    upperRightCoords: z
      .string()
      .regex(/^(\d+)\s+(\d+)$/, 'Invalid coordinates format. Expected format: "number number".')
  })
  .transform(({ upperRightCoords }, ctx) => {
    const parts = upperRightCoords.split(' ');
    if (parts[0] === undefined || parts[1] === undefined) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: 'Invalid coordinates format. Expected format: "number number".'
      });
      return z.NEVER;
    }
    const upperX = parseInt(parts[0], 10);
    const upperY = parseInt(parts[1], 10);

    if (isNaN(upperX) || isNaN(upperY) || upperX === undefined || upperY === undefined) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: 'Coordinates must be numbers.'
      });
      return z.NEVER;
    }

    if (upperX > 50 || upperY > 50) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: 'Coordinates must not be greater than 50.'
      });
      return z.NEVER;
    }

    return { upperX, upperY };
  });

export const OrientationSchema = z.enum(['N', 'E', 'S', 'W']);
export type Orientation = z.infer<typeof OrientationSchema>;
export const Orientations: readonly Orientation[] = ['N', 'E', 'S', 'W'];

export const PositionSchema = z
  .string()
  .regex(/^(\d+)\s+(\d+)\s+([NESW])$/, 'Invalid position and orientation format.')
  .transform((input, ctx) => {
    const [xStr, yStr, orientation] = input.split(' ');
    if (!xStr || !yStr || !orientation) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: 'Invalid position and orientation format.'
      });
      return z.NEVER;
    }
    return {
      x: parseInt(xStr, 10),
      y: parseInt(yStr, 10),
      orientation: orientation as Orientation
    };
  });

export type Position = z.infer<typeof PositionSchema>;

const instructions = ['L', 'R', 'F'] as const;
const InstructionSchema = z.enum(instructions);
const InstructionsSchema = z
  .string()
  .regex(new RegExp(`^([${instructions.join('')}]+)$`), 'Invalid instructions format.')
  .transform((instructions) => instructions.split('').map((instruction) => InstructionSchema.parse(instruction)));
export type Instructions = z.infer<typeof InstructionsSchema>;

export const PositionAndInstructionsSchema = z.object({
  position: PositionSchema,
  instructions: InstructionsSchema
});
