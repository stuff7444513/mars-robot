#!/usr/bin/env node

import { program } from 'commander';
import Enquirer from 'enquirer';
import { Mars } from './Mars.js';
import { MarsCoordinatesSchema, PositionAndInstructionsSchema } from './validators.js';

const { prompt } = Enquirer;

program.command('start').action(async () => {
  console.log('Welcome to Mars');
  await startRobots();
});

const startRobots = async () => {
  const response = await prompt({
    type: 'input',
    name: 'upperRightCoords',
    message: 'Enter the upper-right coordinates of Mars (e.g. 5 3):'
  });

  const result = MarsCoordinatesSchema.safeParse(response);
  if (!result.success) {
    console.error(result.error.issues.map((issue) => issue.message).join('\n'));
    return;
  }
  const mars = new Mars(result.data.upperX, result.data.upperY);

  let addMoreRobots = true;

  while (addMoreRobots) {
    const robotInstructions = await prompt([
      {
        type: 'input',
        name: 'position',
        message: "Enter the robot's starting position and orientation (e.g. 1 1 E):"
      },
      {
        type: 'input',
        name: 'instructions',
        message: "Enter the robot's instructions (e.g., RFRLLFRF):"
      }
    ]);

    const parsedInstructions = PositionAndInstructionsSchema.safeParse(robotInstructions);
    if (!parsedInstructions.success) {
      console.error(parsedInstructions.error.issues.map((issue) => issue.message).join('\n'));
      return;
    }

    mars.addRobot(parsedInstructions.data.position, parsedInstructions.data.instructions);

    const { continueAdding } = await prompt<{ continueAdding: boolean }>({
      type: 'confirm',
      name: 'continueAdding',
      message: 'Would you like to add another robot?',
      initial: false
    });

    addMoreRobots = continueAdding;
  }
  mars.displayResults();
};

program.parse(process.argv);
