import { Instructions, Orientations, Position } from './validators.js';

export class Robot {
  position: Position;
  instructions: Instructions;
  lost: boolean = false;

  constructor(position: Position, instructions: Instructions) {
    this.position = position;
    this.instructions = instructions;
  }

  // execute the instructions stored in the robot
  executeInstructions(scentLocations: Position[], upperX: number, upperY: number) {
    for (const instruction of this.instructions) {
      if (this.lost) {
        break;
      }

      switch (instruction) {
        case 'L':
        case 'R':
          this.rotate(instruction);
          break;
        case 'F':
          this.moveForward(scentLocations, upperX, upperY);
          break;
      }
    }
  }

  private rotate(instruction: 'L' | 'R') {
    const rotationEffect = {
      L: -1,
      R: 1
    };

    const currentOrientationIndex = Orientations.indexOf(this.position.orientation);

    const newIdx = (currentOrientationIndex + rotationEffect[instruction] + Orientations.length) % Orientations.length;
    const newOrientation = Orientations[newIdx];
    if (newOrientation !== undefined) {
      this.position.orientation = newOrientation;
    }
  }

  private moveForward(scentLocations: Position[], upperX: number, upperY: number) {
    // temp position to check for scents
    const newPosition = { ...this.position };

    // move temp position forward
    switch (this.position.orientation) {
      case 'N':
        newPosition.y += 1;
        break;
      case 'S':
        newPosition.y -= 1;
        break;
      case 'E':
        newPosition.x += 1;
        break;
      case 'W':
        newPosition.x -= 1;
        break;
    }

    const isOffGrid = newPosition.x < 0 || newPosition.y < 0 || newPosition.x > upperX || newPosition.y > upperY;
    const isScented = scentLocations.some((scent) => scent.x === this.position.x && scent.y === this.position.y);

    if (isOffGrid) {
      // ignore forward move if the position is offgrid and there is a scent
      if (isScented) {
        return;
      } else {
        // robot is lost :( leave a scent at its last position
        this.lost = true;
        scentLocations.push({ ...this.position });
      }
    } else {
      // must be a valid move, so update position
      this.position = newPosition;
    }
  }
}
