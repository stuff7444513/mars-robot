import { describe, expect, it } from 'vitest';
import { Robot } from '../Robot.js';
import { Instructions, Orientation, Position } from '../validators.js';

describe('Robot', () => {
  describe('executeInstructions', () => {
    const upperX = 5;
    const upperY = 5;
    it('should execute the instructions and update the position correctly', () => {
      const initialPosition = { x: 0, y: 0, orientation: 'N' as Orientation };
      const instructions: Instructions = ['F', 'R', 'F', 'L', 'F'];

      const robot = new Robot(initialPosition, instructions);

      const scentLocations: Position[] = [];

      robot.executeInstructions(scentLocations, upperX, upperY);

      expect(robot.position).toEqual({ x: 1, y: 2, orientation: 'N' });
    });

    it('correctly label a robot as lost', () => {
      const initialPosition = { x: 0, y: 0, orientation: 'N' as Orientation };
      const instructions: Instructions = ['F', 'F', 'F', 'F', 'F', 'F'];

      const robot = new Robot(initialPosition, instructions);

      const scentLocations: Position[] = [];

      robot.executeInstructions(scentLocations, upperX, upperY);

      expect(robot.position).toEqual({ x: 0, y: 5, orientation: 'N' });
      expect(robot.lost).toBe(true);
    });

    it('should ignore instruction with scents correctly', () => {
      const initialPosition = { x: 3, y: 2, orientation: 'N' as Orientation };
      const instructions: Instructions = ['F', 'F', 'F', 'L', 'F', 'F'];

      const robot = new Robot(initialPosition, instructions);

      const scentLocations: Position[] = [{ x: 3, y: 5, orientation: 'N' as Orientation }];

      robot.executeInstructions(scentLocations, upperX, upperY);

      expect(robot.position).toEqual({ x: 1, y: 5, orientation: 'W' });
      expect(robot.lost).toBe(false);
    });
  });
});
