import { describe, expect, it } from 'vitest';
import { MarsCoordinatesSchema, PositionAndInstructionsSchema } from '../validators.js';

describe('validators', () => {
  describe('MarsCoordinatesSchema', () => {
    it('should validate the upper-right coordinates', () => {
      const test = { upperRightCoords: '5 3' };

      const result = MarsCoordinatesSchema.safeParse(test);

      expect(result.success).toBe(true);
      // make ts happy, expect doesn't assert type
      if (result.success === true) {
        expect(result.data).toEqual({ upperX: 5, upperY: 3 });
      }
    });

    it('should fail if the coordinates are not numbers', () => {
      const test = { upperRightCoords: '5 A' };

      const result = MarsCoordinatesSchema.safeParse(test);

      expect(result.success).toBe(false);
      if (result.success === false) {
        expect(result.error.issues).toHaveLength(1);
        expect(result.error.issues?.[0]?.message).toBe('Invalid coordinates format. Expected format: "number number".');
      }
    });

    it('should fail if the coordinates are greater than 50', () => {
      const test = { upperRightCoords: '51 1' };

      const result = MarsCoordinatesSchema.safeParse(test);

      expect(result.success).toBe(false);
      if (result.success === false) {
        expect(result.error.issues).toHaveLength(1);
        expect(result.error.issues?.[0]?.message).toBe('Coordinates must not be greater than 50.');
      }
    });
  });

  describe('PositionAndInstructionsSchema', () => {
    it('should validate the position and instructions of a robot', () => {
      const test = { position: '1 1 E', instructions: 'RFRLLFRF' };

      const result = PositionAndInstructionsSchema.safeParse(test);

      expect(result.success).toBe(true);
      if (result.success === true) {
        expect(result.data).toEqual({
          position: { x: 1, y: 1, orientation: 'E' },
          instructions: ['R', 'F', 'R', 'L', 'L', 'F', 'R', 'F']
        });
      }
    });

    it('should fail if the position is invalid', () => {
      const test = { position: 'A B', instructions: 'RFRLLFRF' };

      const result = PositionAndInstructionsSchema.safeParse(test);

      expect(result.success).toBe(false);
      if (result.success === false) {
        expect(result.error.issues).toHaveLength(1);
        expect(result.error.issues?.[0]?.message).toBe('Invalid position and orientation format.');
      }
    });

    it('should fail if the instructions are invalid', () => {
      const test = { position: '1 1 E', instructions: 'ABABABA' };

      const result = PositionAndInstructionsSchema.safeParse(test);

      expect(result.success).toBe(false);
      if (result.success === false) {
        expect(result.error.issues).toHaveLength(1);
        expect(result.error.issues?.[0]?.message).toBe('Invalid instructions format.');
      }
    });
  });
});
