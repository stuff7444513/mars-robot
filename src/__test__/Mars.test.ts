import { describe, expect, it, vi } from 'vitest';
import { Mars } from '../Mars.js';

describe('Mars', () => {
  it('should add robots', () => {
    const mars = new Mars(5, 3);

    mars.addRobot({ x: 1, y: 1, orientation: 'E' }, ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F']);
    mars.addRobot({ x: 3, y: 2, orientation: 'N' }, ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R']);
    mars.addRobot({ x: 0, y: 3, orientation: 'W' }, ['L', 'L', 'F', 'F', 'F', 'L', 'F', 'L', 'F', 'L']);

    expect(mars.robots).toHaveLength(3);
  });

  it('should display results', () => {
    const logSpy = vi.spyOn(console, 'log');
    const mars = new Mars(5, 3);

    mars.addRobot({ x: 1, y: 1, orientation: 'E' }, ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F']);
    mars.addRobot({ x: 3, y: 2, orientation: 'N' }, ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R']);
    mars.addRobot({ x: 0, y: 3, orientation: 'W' }, ['L', 'L', 'F', 'F', 'F', 'L', 'F', 'L', 'F', 'L']);

    mars.displayResults();

    expect(logSpy.mock?.calls).toHaveLength(3);
    expect(logSpy?.mock?.calls?.[0]?.[0]).toBe('Robot 1: 1 1 E');
    expect(logSpy?.mock?.calls?.[1]?.[0]).toBe('Robot 2: 3 3 N LOST');
    expect(logSpy?.mock?.calls?.[2]?.[0]).toBe('Robot 3: 2 3 S');
  });
});
